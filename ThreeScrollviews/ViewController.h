//
//  ViewController.h
//  ThreeScrollviews
//
//  Created by Andrei Stanescu on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate> {
    UIScrollView* hscroll;
    NSMutableArray* vscrolls;
}
@end
